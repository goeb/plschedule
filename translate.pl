#!/usr/bin/perl

####################################################################################################
#
# Copyright 2008-2010, 2014 Stefan Goebel - <plschedule -at- subtype -dot- de>
#
# This file is part of PlSchedule.
#
# PlSchedule is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# PlSchedule is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with PlSchedule. If not,
# see <http://www.gnu.org/licenses/>.
#
####################################################################################################
#
# Transforms the output of schedule.pl (read from STDIN) to a human readable format.
#
# Usage: schedule.pl [options] | translate.pl [<schedule number>...]
#
# Use one or more <schedule number> arguments to restrict the output to these schedules. If none are
# specified all schedules will be printed. The schedule numbers are shown in the output, ie. run the
# script without any arguments to get a complete list, then use the arguments to show only specific
# schedules if required.
#
####################################################################################################

use strict;
use warnings;

# Do not change the day names if you want to use the html.pl script!
my @day_names = qw( Monday Tuesday Wednesday Thursday Friday Saturday Sunday );
unshift @day_names, undef;

# For odd or even weekly events the following hash maps the input (either 'E' or 'O') to the
# output, eg. "my %weeks = ( 'E' => 'even', 'O' => 'odd');" would display "[even]" or "[odd]" in
# the final output.
my %weeks = ( 'E' => 'E', 'O' => 'O' );

my @schedules;
while (my $line = <STDIN>) {
    chomp $line;
    my @events = split '\), \(', substr $line, 3, -2;
    my $schedule;
        $schedule -> [$_] = [] foreach (1 .. 7);
        foreach (@events) {
            next unless /^(.+), *([AEO]), *(\d), *(\d{3,4}), *(\d{3,4}), *(.*)$/;
            my $event = $2 ne 'A' ? $1 . qq( [$weeks{$2}]) : $1;
            $event .= " | $6" if ($6);
            push @{$schedule -> [$3]}, {
                'event'	=> $event,
                'begin'	=> sprintf ('%04d', $4),
                'end'	=> sprintf ('%04d', $5),
            };
        }
        push @schedules, $schedule if $schedule;
}

sub print_event {
    my ($event, $begin, $end) = @_;
    my ($bh, $bm) = (substr ($begin, 0, 2), substr ($begin, -2));
    my ($eh, $em) = (substr ($end,   0, 2), substr ($end,   -2));
    print "\t\t", $bh, ':', $bm, ' - ', $eh, ':', $em, ' => ', $event, "\n";
}

sub print_schedules {
    my $number = 0;
    my %print;
    @print {@ARGV} = ();
    foreach my $schedule (@_) {
        ++ $number;
        next if @ARGV and not exists $print {$number};
        printf "Schedule #%05d:\n\n", $number;
        my $day_of_week = 0;
        foreach my $day (@$schedule) {
            next unless $day;
            print "\t", $day_names [++ $day_of_week], "\n";
            foreach (sort { $a -> {'begin'} <=> $b -> {'begin'} } @$day) {
                print_event ($_ -> {'event'}, $_ -> {'begin'}, $_ -> {'end'});
            }
            print "\n";
        }
        print "\n";
    }
}

print_schedules (@schedules);

# :indentSize=4:tabSize=4:noTabs=true:mode=perl:maxLineLen=100: