%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2008-2010, 2014 Stefan Goebel - <plschedule -at- subtype -dot- de>
%
% This file is part of PlSchedule.
%
% PlSchedule is free software: you can redistribute it and/or modify it under the terms of the GNU
% General Public License as published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% PlSchedule is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
% even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
% General Public License for more details.
%
% You should have received a copy of the GNU General Public License along with PlSchedule. If not,
% see <http://www.gnu.org/licenses/>.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% An event must be defined as «event( <event name>, <week>, <day>, <start>, <end>, <info> ):
%
% Example:      event( 'E01', 'A', 1, 1100, 1230, '' ).
%               event( 'E02', 'E', 1, 1300, 1430, '' ).  event( 'E02', 'O', 1, 1500, 1630, '' ).
%               event( 'E03', 'E', 1, 1500, 1630, '' ).
%               event( 'E04', 'E', 1, 1300, 1430, '' ).  event( 'E04', 'O', 1, 1500, 1630, '' ).
%
% The example also shows how to define alternatives for an event: all events with the same name are
% considered to be interchangeable, and of all interchangeable events only one will be included in
% every generated schedule. The value for <week> must be one of 'A', 'E' or 'O', for events occuring
% in all weeks, even weeks or odd weeks, respectively. <info> may be any arbitrary string (it is
% actually ignored, but included in the output). If you use the html.pl and/or xhtml.pl scripts to
% create HTML schedules, a vertical bar (or pipe symbol, '|') in the info string will be replaced by
% a newline. Optional events are currently not supported!
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% A more complex example (this will generate the example.html file included in the distribution).
% There are 54 possible weekly schedules for the following events:

event( 'Event 1', 'A', 1, 1300, 1430, 'Note 1'            ).

event( 'Event 2', 'A', 1, 1100, 1230, 'Line 1 | Line 2'   ).
event( 'Event 2', 'A', 4, 0900, 1030, 'Line 1 | Line 2'   ).

event( 'Event 3', 'A', 1, 1700, 1830, 'Note 3'            ).

event( 'Event 4', 'E', 2, 0900, 1030, 'Note 4.1'          ).
event( 'Event 4', 'E', 4, 1300, 1430, 'Note 4.2 | Line 2' ).
event( 'Event 4', 'O', 4, 1300, 1430, 'Note 4.3'          ).

event( 'Event 5', 'A', 2, 1100, 1230, 'Note 5'            ).

event( 'Event 6', 'E', 5, 0900, 1030, 'Some note...'      ).
event( 'Event 6', 'O', 5, 0900, 1030, 'Another note...'   ).
event( 'Event 6', 'O', 5, 1300, 1430, 'Last note...'      ).

event( 'Event 8', 'O', 1, 0700, 0830, 'abc | def | ghi'   ).
event( 'Event 8', 'E', 4, 1700, 1830, 'jkl | mno | pqr'   ).
event( 'Event 8', 'O', 4, 1700, 1830, 'stu | vwx | yz.'   ).

event( 'Event 9', 'A', 4, 1100, 1230, ''                  ).

% :indentSize=4:tabSize=4:noTabs=true:mode=prolog:maxLineLen=100: