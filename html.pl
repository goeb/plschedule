#!/usr/bin/perl

####################################################################################################
#
# Copyright 2008-2010, 2014 Stefan Goebel - <plschedule -at- subtype -dot- de>
#
# This file is part of PlSchedule.
#
# PlSchedule is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# PlSchedule is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with PlSchedule. If not,
# see <http://www.gnu.org/licenses/>.
#
####################################################################################################
#
# Transforms the output of translate.pl (read from STDIN) to a HTML table. Requires the
# Calendar::Schedule module.
#
# Usage: schedule.pl [options] | translate.pl [options] | html.pl
#
# Redirect the output to a file if required. Note: The original input (i.e. the output of the
# translate.pl script) will be printed to STDERR, redirect it to /dev/null if required. In order to
# get valid output, you must not use any HTML special characters in the configuration (like < or >)!
#
####################################################################################################

use strict;
use warnings;

use Calendar::Schedule;

my ($start, $stop, $override_start, $day, $schedule) = (24, 0, 7);

$| = 1;

while (my $line = <STDIN>) {
    if ($line =~ /^Schedule #(\d+):$/) {
        print STDERR "\nSchedule #$1\n";
        ($schedule, $start, $stop, $day) = push_schedule ($schedule, $start, $stop);
    }
    elsif ($line =~ /^\t([a-z]+)$/i) {
        print STDERR "\n\t$1\n";
        $day = substr $1, 0, 3;
    }
    elsif ($line =~ /^\t\t(\d\d:\d\d) - (\d\d:\d\d) => (.+)$/) {
        $start = int (substr ($1, 0, 2)) if int (substr ($1, 0, 2)) < $start;
        $stop  = int (substr ($2, 0, 2)) if int (substr ($2, 0, 2)) > $stop;
        print STDERR "\t\t$1 - $2 => $3\n";
        my ($a, $o, $e) = ($1, $2, $3);
        $e =~ s{ \| }{<br />}g;
        $schedule -> add_entry ("$day $a-$o", $e) if $day;
    }
}

push_schedule ($schedule, $start, $stop);

sub push_schedule {
    my ($schedule, $start, $stop) = @_;
    if ($schedule) {
        if (defined $start and defined $stop) {
            $start = $override_start if $override_start and $override_start < $start;
            # Hack to display a continuous table with rows for every thirty minutes.
            # This may not work in all cases, works fine if you stick to events
            # starting at */0 or */30.
            my @rowlabels;
            for ($start .. $stop) {
                push @rowlabels, sprintf ('%02i:00', $_);
                push @rowlabels, sprintf ('%02i:30', $_);
            }
            $schedule -> {'DefaultRowLabels'} = [ @rowlabels ];
            $schedule -> {'RowLabels'}        = [ @rowlabels ];
        }
        print $schedule -> generate_table ();
    }
    $schedule = Calendar::Schedule -> new ();
    $schedule -> set_ColLabel ('%A');
    return ($schedule, 24, 0);
}

# :indentSize=4:tabSize=4:noTabs=true:mode=perl:maxLineLen=100: