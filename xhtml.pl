#!/usr/bin/perl

####################################################################################################
#
# Copyright 2008-2010, 2014 Stefan Goebel - <plschedule -at- subtype -dot- de>
#
# This file is part of PlSchedule.
#
# PlSchedule is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# PlSchedule is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with PlSchedule. If not,
# see <http://www.gnu.org/licenses/>.
#
####################################################################################################
#
# Transforms the output of html.pl (read from STDIN) to valid XHTML 1.0 Strict. Requires the
# HTML::Parser module.
#
# Usage: schedule.pl [options] | translate.pl [options] | html.pl | xhtml.pl
#
# The XHTML output will be printed to STDOUT, redirect it to a file if required. Note: In order to
# get valid output, you must not use any XML special characters in the configuration (like < or >)!
#
####################################################################################################

use strict;
use warnings;

use HTML::Parser ();

my $p = HTML::Parser -> new (
    'api_version'       => 3,
    'start_h'           => [ \&start_tag, "tagname, attr" ],
    'end_h'             => [ \&end_tag,   "tagname"       ],
    'text_h'            => [ \&text,      "text"          ],
    'marked_sections'   => 1,
    'unbroken_text'     => 1,
);

my ($schedule, $column, $break) = (0, 0, 0);

print <DATA>;

$p -> parse ($_) while (<STDIN>);
$p -> eof ();

print '</body></html>';

sub start_tag {
    my ($tag, $attr, @attrs, @classes) = @_;
    if ($tag eq 'table') {
        my $break_class = $break ++ ? ' class="break"' : '';
        print "<h1$break_class>Schedule #", ++ $schedule, '</h1>';
    }
    elsif ($tag eq 'td' or $tag eq 'th') {
        push @attrs,  q(class="time")               if $column ++ == 0;
        push @attrs, qq(rowspan="$attr->{rowspan}") if $attr -> {'rowspan'};
        push @attrs, qq(colspan="$attr->{colspan}") if $attr -> {'colspan'};
        push @attrs,  q(class="event")              if $attr -> {'bgcolor'};
    }
    elsif ($tag eq 'br') {
        print '<br />';
        return;
    }
    print @attrs ? "<$tag " . join (' ', @attrs) . '>' : "<$tag>";
}

sub end_tag {
    my ($tag) = @_;
    $column = 0 if $tag eq 'tr';
    print "</$tag>";
}

sub text {
    chomp (my $text = shift);
    print $text if $text and $text ne '&nbsp;';
}

__DATA__
<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">

<head>
    <title>Schedules</title>
    <style type="text/css">
        body { font-size: 0.8em; }
        h1 { font-size: 1.3em; font-weight: normal; letter-spacing: 0.3em; word-spacing: 1.3em; }
        h1.break { page-break-before: always; }
        table { border-collapse: collapse; empty-cells: show; margin-bottom: 5em; table-layout: fixed; width: 100%; }
        td, th { border: 1px solid #000; overflow: hidden; padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: middle; }
        td.time { width: 5em; }
        td.event { background-color: #ffd; }
    </style>
</head>

<body>

<!-- :indentSize=4:tabSize=4:noTabs=true:mode=perl:maxLineLen=100: -->