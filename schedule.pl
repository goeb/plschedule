#!/usr/bin/prolog -q -g "run( X ), writeln( X ), fail" -t halt -s

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright 2008-2010, 2014 Stefan Goebel - <plschedule -at- subtype -dot- de>
%
% This file is part of PlSchedule.
%
% PlSchedule is free software: you can redistribute it and/or modify it under the terms of the GNU
% General Public License as published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% PlSchedule is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
% even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
% General Public License for more details.
%
% You should have received a copy of the GNU General Public License along with PlSchedule. If not,
% see <http://www.gnu.org/licenses/>.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Generates all possible schedules. Requires SWI Prolog!
%
% Usage: schedule.pl <config file> [<config file> ...]
%
% At least one config file argument is mandatory, note that consult/1 is used to load the file, ie.
% all prolog statements inside the file will be executed (!), and it must be a valid prolog file.
%
% An example configuration file can be found in the "example" folder. Do not use configuration files
% from untrusted sources!
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

run( X ) :-
    current_prolog_flag( argv, All ),
    append( _, [--|Args], All ),
    !,
    load_config( Args ),
    find_schedule( X ).

load_config( [] ) :-
    writeln( 'Please specify a configuration file!' ),
    halt( 1 ).

load_config( Files ) :-
    load_file( Files ).

load_file( [] ).

load_file( [File | Files] ) :-
    consult( File ),
    load_file( Files ).

find_schedule( X ) :-
    all_events( Events ),
    find_schedule( Events, X ).

find_schedule( [], [] ).

find_schedule( [ Event | Events ], X ) :-
    event( Event, Week, Day, Begin, End, Info ),
    find_schedule( Events, Y ),
    free_slot( Y, Week, Day, Begin, End ),
    append( Y, [ ( Event, Week, Day, Begin, End, Info ) ], X ).

free_slot( [], _, _, _, _ ).

free_slot( [ ( _, W1, D1, B1, E1, _ ) | T ], W2, D2, B2, E2 ) :-
    not_overlapping( W1, D1, B1, E1, W2, D2, B2, E2 ),
    free_slot( T, W2, D2, B2, E2 ).

not_overlapping( _, D1, _, _, _, D2, _, _ ) :-
    D1 \= D2,
    !.

not_overlapping( W1, _, _, _, W2, _, _, _ ) :-
    (
        ( W1 = 'E' , W2 = 'O' ) ;
        ( W1 = 'O' , W2 = 'E' )
    ),
    !.

not_overlapping( _, _, B1, E1, _, _, B2, E2 ) :-
    \+ (
        ( B1 < B2 , E1 > B2 ) ;
        ( B2 < B1 , E2 > B1 ) ;
        ( B1 = B2 , E1 = E2 )
    ).

all_events( X ) :-
    findall( C, event( C, _, _, _, _, _ ), Y ),
    list_to_set( Y, X ).

?- set_prolog_flag( toplevel_print_options, [ quoted( true ), portray( true ), max_depth( 0 ) ] ).

% :indentSize=4:tabSize=4:noTabs=true:mode=prolog:maxLineLen=100: